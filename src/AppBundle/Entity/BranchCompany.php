<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as AssertBridge;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Branch company entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BranchCompanyRepository")
 * @ORM\Table(name="company_branches", indexes={ @ORM\Index(name="name_idx", columns={"name"})})
 * @package AppBundle\Entity
 */
class BranchCompany
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false, length=255 )
     * @Assert\NotBlank(message="branch_company.name.blank")
     * @Assert\Length(min=3, max=75, minMessage="branch_company.name.short", maxMessage="branch_company.name.long" )
     */
    protected $name;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="BranchCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @Assert\NotBlank(message="branch_company.company.blank")
     */
    protected $company;

    /**
     * Returning human readable entity
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string|null $name
     * @return BranchCompany
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company
     *
     * @param Company|null $company
     * @return BranchCompany
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return BranchCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }
}
