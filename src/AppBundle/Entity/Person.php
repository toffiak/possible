<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as AssertBridge;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 * @ORM\Table(name="people",
 *     indexes={
 *         @ORM\Index(name="first_name_idx", columns={"first_name"}),
 *         @ORM\Index(name="last_name_idx", columns={"last_name"}),
 *         @ORM\Index(name="sex_idx", columns={"sex"}),
 *     }
 * )
 * @package AppBundle\Entity
 */
class Person
{
    /**
     * @var int
     */
    const MALE = 1;

    /**
     * @var int
     */
    const FEMALE = 2;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", nullable=false, length=75 )
     * @Assert\NotBlank(message="person.first_name.blank")
     * @Assert\Length(min=1, max=75, minMessage="person.first_name.short", maxMessage="person.first_name.long" )
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", nullable=false, length=75 )
     * @Assert\NotBlank(message="person.last_name.blank")
     * @Assert\Length(min=1, max=75, minMessage="person.last_name.short", maxMessage="person.last_name.long" )
     */
    protected $lastName;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="birthday", type="date", nullable=true )
     * @Assert\Date(message="person.birthday.invalid" )
     */
    protected $birthday;

    /**
     * @var int
     * @ORM\Column(name="sex", type="smallint", nullable=true )
     */
    protected $sex;

    /**
     * @var Place
     * @ORM\ManyToOne(targetEntity="Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     * @Assert\NotBlank(message="person.place.blank")
     */
    protected $place;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @Assert\NotBlank(message="person.company.blank")
     */
    protected $company;

    /**
     * @var BranchCompany
     * @ORM\ManyToOne(targetEntity="BranchCompany")
     * @ORM\JoinColumn(name="branch_company_id", referencedColumnName="id")
     * @Assert\NotBlank(message="person.branch_company.blank")
     */
    protected $branchCompany;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthday
     *
     * @param \DateTime|null $birthday
     * @return Person
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set sex
     *
     * @param integer|null $sex
     * @return Person
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer|null
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set place
     *
     * @param Place|null $place
     * @return Person
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return Place|null
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set company
     *
     * @param Company|null $company
     * @return Person
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set branchCompany
     *
     * @param BranchCompany|null $branchCompany
     * @return Person
     */
    public function setBranchCompany($branchCompany)
    {
        $this->branchCompany = $branchCompany;

        return $this;
    }

    /**
     * Get branchCompany
     *
     * @return BranchCompany|null
     */
    public function getBranchCompany()
    {
        return $this->branchCompany;
    }

    /**
     * Returning person age (in years)
     *
     * @return int|null
     */
    public function getAge()
    {
        if (empty($this->birthday) || !$this->birthday instanceof \DateTime) {
            return null;
        }

        return (new \DateTime())->diff($this->birthday)->y;
    }
}
