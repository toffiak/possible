<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as AssertBridge;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Place entity
 *
 * @ORM\Entity
 * @ORM\Table(name="places", indexes={ @ORM\Index(name="name_idx", columns={"name"})})
 * @package AppBundle\Entity
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false, length=255 )
     * @Assert\NotBlank(message="place.first_name.blank")
     * @Assert\Length(min=3, max=75, minMessage="place.name.short", maxMessage="place.name.long" )
     */
    protected $name;

    /**
     * Returning human readable entity
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
}
