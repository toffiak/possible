<?php

namespace AppBundle\EventSubscribers;

use AppBundle\AppEvents;
use AppBundle\Entity\Person;
use AppBundle\Event\PersonEvent;
use AppBundle\Model\SexGuesser;
use Doctrine\ORM\EntityManagerInterface;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AppSubscriber
 *
 * @JMS\Service
 * @JMS\Tag("kernel.event_subscriber")
 * @package AppBundle\EventSubscribers
 */
class AppSubscriber implements EventSubscriberInterface
{
    /**
     * @var SexGuesser
     * @JMS\Inject("sex.guesser")
     */
    public $sexGuesser;

    /**
     * @var EntityManagerInterface
     * @JMS\Inject("doctrine.orm.entity_manager")
     */
    public $entityManager;

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            AppEvents::PERSON_CREATED => ['personCreated'],
            AppEvents::PERSON_UPDATED => ['personUpdated']
        ];
    }

    /**
     * Person created
     *
     * @param PersonEvent $event
     */
    public function personCreated(PersonEvent $event)
    {
        $person = $event->getPerson();
        $this->setPersonSex($person);
    }

    /**
     * Person updated
     *
     * @param PersonEvent $event
     */
    public function personUpdated(PersonEvent $event)
    {
        /* Same as person created */
        $this->personCreated($event);
    }

    /**
     * Setting person sex depends on first name
     *
     * @param Person $person
     */
    private function setPersonSex(Person $person)
    {
        $sex = $this->sexGuesser->guessByFirstName($person->getFirstName());
        if ($person->getSex() !== $sex) {
            $person->setSex($sex);
            $this->entityManager->persist($person);
            $this->entityManager->flush($person);
        }
    }
}