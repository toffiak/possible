<?php

namespace AppBundle;

/**
 * Class AppEvents
 *
 * @package AppBundle
 */
final class AppEvents
{
    /**
     * @var string
     */
    const PERSON_CREATED = 'person_created';

    /**
     * @var string
     */
    const PERSON_UPDATED = 'person_updated';
}