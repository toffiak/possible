<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Branch company controller.
 *
 * @Configuration\Route("branch/company")
 * @package AppBundle\Controller
 */
class BranchCompanyController extends Controller
{
    /**
     * @var string
     */
    const STATUS_OK = 'OK';

    /**
     * @var string
     */
    const STATUS_FAIL = 'FAIL';

    /**
     * Lists all person entities.
     *
     * @Configuration\Route("/by/company/{company}",
     *     name="company_branches_by_company",
     *     requirements={"company"="\d+"},
     *     options = { "expose" = true }
     * )
     * @Configuration\Method("GET")
     * @param Company $company
     * @return JsonResponse
     */
    public function companyBranchesByCompanyAction(Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $companyBranches = $em->getRepository('AppBundle:BranchCompany')->findCompanyBranchesByCompany($company);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => self::STATUS_FAIL]);
        }

        return new JsonResponse(['status' => self::STATUS_OK, 'company_branches' => $companyBranches]);
    }
}