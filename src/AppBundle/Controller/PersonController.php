<?php

namespace AppBundle\Controller;

use AppBundle\AppEvents;
use AppBundle\Entity\Person;
use AppBundle\Event\PersonEvent;
use AppBundle\Form\Type\DeletePersonType;
use AppBundle\Form\Type\PersonType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Configuration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Person controller.
 *
 * @Configuration\Route("person")
 * @package AppBundle\Controller
 */
class PersonController extends Controller
{
    /**
     * Lists all person entities.
     *
     * @Configuration\Route("/{page}", name="people_list", requirements={"page"="\d+"}, defaults={"page"=1})
     * @Configuration\Method("GET")
     * @Configuration\Template(template="AppBundle:Person:list.html.twig")
     * @param mixed $page
     * @return Response
     */
    public function listAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $em->getRepository('AppBundle:Person')->findAllPeopleQuery(),
            $page,
            $this->getParameter('nb_of_items_on_list')
        );
        $deleteForm = $this->createForm(DeletePersonType::class, null, ['people' => $pagination->getItems()]);

        return ['pagination' => $pagination, 'delete_form' => $deleteForm->createView()];
    }

    /**
     * Creates a new person entity.
     *
     * @Configuration\Route("/new", name="person_new")
     * @Configuration\Method({"GET", "POST"})
     * @Configuration\Template(template="AppBundle:Person:new.html.twig")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->processForm($person);

            return $this->redirectToRoute('people_list');
        }

        return ['person' => $person, 'form' => $form->createView()];
    }

    /**
     * Displays a form to edit an existing person entity.
     *
     * @Configuration\Route("/{id}/edit", name="person_edit")
     * @Configuration\Method({"GET", "POST"})
     * @Configuration\Template(template="AppBundle:Person:edit.html.twig")
     * @param Request $request
     * @param Person $person
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Person $person)
    {
        $editForm = $this->createForm(PersonType::class, $person);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->processForm($person);

            return $this->redirectToRoute('people_list');
        }

        return ['person' => $person, 'form' => $editForm->createView()];
    }

    /**
     * Deleting people
     *
     * @Configuration\Route("/delete", name="person_delete")
     * @Configuration\Method("DELETE")
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $deleteForm = $this->createForm(DeletePersonType::class, null, ['people' => []]);
        $deleteForm->handleRequest($request);
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $this->processDeleteForm($deleteForm);
        }

        return $this->redirectToRoute('people_list');
    }

    /**
     * Processing form
     *
     * @param Person $person
     */
    private function processForm(Person $person)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($person);
        $eventName = empty($person->getId()) ? AppEvents::PERSON_CREATED : AppEvents::PERSON_UPDATED;
        try {
            $conn = $this->getDoctrine()->getConnection();
            $conn->beginTransaction();
            $em->flush();
            $this->get('event_dispatcher')->dispatch($eventName, new PersonEvent($person));
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollback();
            $this->get('logger')->error($e->getMessage());
        }
    }

    /**
     * Processing delete form
     *
     * @param FormInterface $deleteForm
     */
    private function processDeleteForm(FormInterface $deleteForm)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            foreach ($deleteForm->getExtraData() as $personId => $v) {
                $em->remove($em->getReference('AppBundle:Person', $personId));
            }
            $em->flush();
        } catch (\Exception $e) {
            $this->get('logger')->error($e->getMessage());
        }
    }
}