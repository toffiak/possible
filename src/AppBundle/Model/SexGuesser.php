<?php

namespace AppBundle\Model;

use AppBundle\Entity\Person;
use JMS\DiExtraBundle\Annotation as JMS;

/**
 * Class SexGuesser
 *
 * @JMS\Service("sex.guesser")
 * @package AppBundle\Model
 */
class SexGuesser
{
    /**
     * Guessing sex by first name
     *
     * @param string $firstName
     * @return int
     */
    public function guessByFirstName(string $firstName)
    {
        if ('a' === mb_substr($firstName, -1)) {
            return Person::FEMALE;
        }

        return Person::MALE;
    }
}