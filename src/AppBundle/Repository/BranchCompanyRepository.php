<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Company;
use Doctrine\ORM\EntityRepository;

/**
 * Class BranchCompanyRepository
 *
 * @package AppBundle\Repository
 */
class BranchCompanyRepository extends EntityRepository
{
    /**
     * Finding all company branches by a given company
     *
     * Depends on "asArray" parameter we return QueryBuilder instance or array with branch company
     * formatted as key => value array, where key is BranchCompany entity id and value is a name.
     *
     * @param Company $company
     * @param bool $asArray
     * @return array
     */
    public function findCompanyBranchesByCompany(Company $company, $asArray = true)
    {
        $qb = $this->prepareQueryBuilder($company);
        if (true === $asArray) {
            $results = $qb->getQuery()->getScalarResult();
            if (empty($results)) {
                return [];
            }

            return array_column($results, 'bc_name', 'bc_id');
        } else {
            return $qb;
        }
    }

    /**
     * Returning doctrine query builder used for finding BranchCompany entities with a given data.
     *
     * @param array $data
     * @return \Doctrine\ORM\Query
     */
    public function getBranchCompanyQueryByData(array $data)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb->addSelect('bc')
            ->from('AppBundle:BranchCompany', 'bc')
            ->leftJoin('bc.company', 'c')
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('c', ':company_id'),
                    $qb->expr()->eq('bc', ':branch_company_id')
                )
            )
            ->setParameters($data);
    }

    /**
     * Preparing query builder used for finding all company branches by company
     *
     * @param Company $company
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function prepareQueryBuilder(Company $company)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->addSelect('bc')
            ->from('AppBundle:BranchCompany', 'bc')
            ->leftJoin('bc.company', 'c')
            ->andWhere($qb->expr()->eq('c', ':company_id'))
            ->setParameter('company_id', $company->getId());

        return $qb;
    }
}