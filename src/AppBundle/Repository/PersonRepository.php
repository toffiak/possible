<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class PersonRepository
 *
 * @package AppBundle\Repository
 */
class PersonRepository extends EntityRepository
{
    /**
     * Finding all existing people as Doctrine query
     *
     * @return \Doctrine\ORM\Query
     */
    public function findAllPeopleQuery()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->addSelect('p')
            ->from('AppBundle:Person', 'p')
            ->orderBy('p.id', 'DESC')
            ->getQuery();
    }
}