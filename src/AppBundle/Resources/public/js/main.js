(function () {

    $(document).ready(function () {

        /* Datepicker configuration  */
        $('#appbundle_person_birthday').datepicker({
            format: "dd-mm-yyyy",
            language: 'pl',
            maxViewMode: 2,
            endDate: "today"
        });

        /**
         * On person company change
         */
        $('#appbundle_person_company').on('change', function () {
            var selectedCompany = $(this).val(),
                branchCompanyField = $('#appbundle_person_branchCompany');
            if (typeof selectedCompany !== 'undefined' && selectedCompany !== null && selectedCompany !== '') {
                $.ajax({
                    url: Routing.generate('company_branches_by_company', {company: selectedCompany}),
                    method: "GET"
                }).done(function (data) {
                    if (data.status === 'OK') {
                        fillSelectField(branchCompanyField, data.company_branches);
                        branchCompanyField.attr('disabled', false);
                    }
                })
            } else {
                fillSelectField(branchCompanyField, []);
                branchCompanyField.attr('disabled', true);
            }
        });

    });

    /**
     * Filling select field with given data, removing all options except first one
     *
     * @param field
     * @param data
     */
    function fillSelectField(field, data) {
        field.find('option:gt(0)').remove();
        $.each(data, function (k, v) {
            field.append($('<option/>', {
                value: k,
                text: v
            }));
        });
    }

})();
