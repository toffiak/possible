<?php

namespace AppBundle\Form\EventListener;

use AppBundle\Entity\BranchCompany;
use AppBundle\Repository\BranchCompanyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class BranchCompanySubscriber
 *
 * @package AppBundle\Form\EventListener
 */
class BranchCompanySubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::POST_SET_DATA => 'onPostSetData', FormEvents::PRE_SUBMIT => 'onPreSubmit'];
    }

    /**
     * On pre submit
     *
     * @param FormEvent $event
     */
    public function onPreSubmit(FormEvent $event)
    {
        $person = $event->getData();
        $form = $event->getForm();

        /**
         * Validator expect that given "branchCompany" value will be on list of expected values, but we do do give
         * any of those values, so we need to overwrite "branchCompany" field and inform that we expect value
         * same as already given one in request data and our branch company must also belongs to company, also given
         * in request data.
         */
        if (
            isset($person['branchCompany']) &&
            !empty($branchCompanyId = $person['branchCompany']) &&
            !empty($companyId = $person['company'])
        ) {
            $data = ['company_id' => $companyId, 'branch_company_id' => $branchCompanyId];
            $form->add('branchCompany', EntityType::class, [
                'label' => 'person.branch_company',
                'required' => false,
                'class' => BranchCompany::class,
                'query_builder' => function (BranchCompanyRepository $er) use ($data) {
                    return $er->getBranchCompanyQueryByData($data);
                }
            ]);
        }
    }

    /**
     * On post set data
     *
     * @param FormEvent $event
     */
    public function onPostSetData(FormEvent $event)
    {
        $person = $event->getData();
        $form = $event->getForm();

        /**
         * For creating people "branchCompany" field must be empty, it depends on selected company, for editing
         * person it must be filled with available company branches for selected company.
         */
        if (empty($person->getId())) {
            $form->add('branchCompany', EntityType::class, [
                'label' => 'person.branch_company',
                'required' => false,
                'class' => BranchCompany::class,
                'attr' => ['disabled' => true],
                'choices' => [],
            ]);
        } else {
            $company = $person->getCompany();
            $form->add('branchCompany', EntityType::class, [
                'label' => 'person.branch_company',
                'required' => false,
                'class' => BranchCompany::class,
                'query_builder' => function (BranchCompanyRepository $er) use ($company) {
                    return $er->findCompanyBranchesByCompany($company, false);
                }
            ]);
        }
    }
}