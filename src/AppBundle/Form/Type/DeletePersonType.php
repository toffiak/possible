<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeletePersonType
 *
 * @package AppBundle\Form\Type
 */
class DeletePersonType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['people'] as $person) {
            $builder->add($person->getId(), SubmitType::class, [
                'label' => 'global.delete',
                'attr' => ['data' => 'person-id=' . $person->getId()]
            ]);
        }
        $builder->setMethod('DELETE');
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'people' => null,
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_person_delete';
    }
}