<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Person;
use AppBundle\Form\EventListener\BranchCompanySubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonType
 *
 * @package AppBundle\Form\Type
 */
class PersonType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new BranchCompanySubscriber());
        $builder
            ->add('lastName', TextType::class, [
                'label' => 'person.last_name'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'person.first_name',
                'required' => false,
            ])
            ->add('place', null, [
                'label' => 'person.place',
                'required' => false,
            ])
            ->add('birthday', DateType::class, [
                'label' => 'person.birthday',
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
            ])
            ->add('company', null, [
                'label' => 'person.company',
                'required' => false,
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\Person']);
    }

    /**
     * {@inheritDoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_person';
    }
}