<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Company;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCompanyData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCompanyData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $company1 = new Company();
        $company1->setName('INTEL');

        $company2 = new Company();
        $company2->setName('AMD');

        $manager->persist($company1);
        $manager->persist($company2);
        $manager->flush();

        $this->addReference('company-intel', $company1);
        $this->addReference('company-amd', $company2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}