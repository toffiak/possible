<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\BranchCompany;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadBranchCompanyData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadBranchCompanyData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $branchCompany1 = new BranchCompany();
        $branchCompany1->setName('oddział w Polsce');
        $branchCompany1->setCompany($this->getReference('company-intel'));

        $branchCompany2 = new BranchCompany();
        $branchCompany2->setName('oddział w Niemczech');
        $branchCompany2->setCompany($this->getReference('company-intel'));

        $branchCompany3 = new BranchCompany();
        $branchCompany3->setName('oddział w Czechach');
        $branchCompany3->setCompany($this->getReference('company-amd'));

        $branchCompany4 = new BranchCompany();
        $branchCompany4->setName('oddział w Słowacji');
        $branchCompany4->setCompany($this->getReference('company-amd'));

        $branchCompany5 = new BranchCompany();
        $branchCompany5->setName('oddział w Polsce');
        $branchCompany5->setCompany($this->getReference('company-amd'));

        $manager->persist($branchCompany1);
        $manager->persist($branchCompany2);
        $manager->persist($branchCompany3);
        $manager->persist($branchCompany4);
        $manager->persist($branchCompany5);
        $manager->flush();

        $this->addReference('branch-company-polska', $branchCompany1);
        $this->addReference('branch-company-niemcy', $branchCompany2);
        $this->addReference('branch-company-czechy', $branchCompany3);
        $this->addReference('branch-company-slowacja', $branchCompany4);
        $this->addReference('branch-company-polska-2', $branchCompany5);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}