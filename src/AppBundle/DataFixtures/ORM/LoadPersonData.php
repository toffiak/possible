<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Person;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPersonData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadPersonData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $person1 = new Person();
        $person1->setFirstName('Janusz');
        $person1->setLastName('Zieliński');
        $person1->setSex(Person::MALE);
        $person1->setBirthday(\DateTime::createFromFormat('Y-m-d','1975-05-02'));
        $person1->setPlace($this->getReference('place-warszawa'));
        $person1->setCompany($this->getReference('company-intel'));
        $person1->setBranchCompany($this->getReference('branch-company-niemcy'));

        $person2 = new Person();
        $person2->setFirstName('Ewa');
        $person2->setLastName('Nosowska');
        $person2->setSex(Person::FEMALE);
        $person2->setBirthday(\DateTime::createFromFormat('Y-m-d','1989-10-09'));
        $person2->setPlace($this->getReference('place-poznan'));
        $person2->setCompany($this->getReference('company-amd'));
        $person2->setBranchCompany($this->getReference('branch-company-slowacja'));

        $manager->persist($person1);
        $manager->persist($person2);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}