<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Place;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPlaceData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadPlaceData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $place1 = new Place();
        $place1->setName('Warszawa');

        $place2 = new Place();
        $place2->setName('Toruń');

        $place3 = new Place();
        $place3->setName('Poznań');

        $manager->persist($place1);
        $manager->persist($place2);
        $manager->persist($place3);
        $manager->flush();

        $this->addReference('place-warszawa', $place1);
        $this->addReference('place-poznan', $place3);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}