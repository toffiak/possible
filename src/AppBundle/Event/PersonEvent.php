<?php

namespace AppBundle\Event;

use AppBundle\Entity\Person;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class PersonEvent
 *
 * @package AppBundle\Event
 */
class PersonEvent extends Event
{
    /**
     * @var null|Person
     */
    private $person;

    /**
     * PersonEvent constructor.
     *
     * @param null|Person $person
     */
    public function __construct($person)
    {
        $this->person = $person;
    }

    /**
     * Get document
     *
     * @return null|Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set document
     *
     * @param Person $person
     * @return PersonEvent
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }
}